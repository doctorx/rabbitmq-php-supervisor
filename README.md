## Install

* Vagrant
* Ansible

### Run

  cd /project/path/
  vagrant up

## Servers

### RabbitMQ
http://192.168.33.62:15672/

login: guest
password: guest

### Supervisor

http://192.168.33.62:9080/