<?php
/**
 * Created by PhpStorm.
 * User: dx
 * Date: 01.06.15
 * Time: 11:42
 */

namespace Ristretto\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;


class OrderGeneratorCommand extends Command {

    protected $limitOrders = 30;

    /**
     * @param int $limitOrders
     */
    public function setLimitOrders($limitOrders)
    {
        $this->limitOrders = $limitOrders;
    }

    /**
     * @return int
     */
    public function getLimitOrders()
    {
        return $this->limitOrders;
    }

    protected function configure()
    {
        $this
            ->setName('order:generate')
            ->setDescription('Generate random orders')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->sendInQueue($this->generateOrders());

    }

    protected function generateOrders()
    {
        $status = array('approved', 'decline');
        $faker = \Faker\Factory::create();

        $orders = array();
        for ($i=0; $i < 30; $i++) {
            $order = array(
                'order_num' => $faker->uuid,
                'email' => $faker->freeEmail,
                'client_ip' => $faker->ipv4,
                'customer' =>$faker->name,
                'fstatus' => $status[array_rand($status)]
            );

            $orders[] = $order;
        }

        return $orders;
    }

    protected function sendInQueue($orders)
    {
        $connection = new AMQPConnection('localhost', 5672, 'guest', 'guest');
        $channel = $connection->channel();

        $channel->queue_declare(
            'order_queue',	#queue name - Имя очереди может содержать до 255 байт UTF-8 символов
            false,      	#passive - может использоваться для проверки того, инициирован ли обмен, без того, чтобы изменять состояние сервера
            true,      	#durable - убедимся, что RabbitMQ никогда не потеряет очередь при падении - очередь переживёт перезагрузку брокера
            false,      	#exclusive - используется только одним соединением, и очередь будет удалена при закрытии соединения
            false       	#autodelete - очередь удаляется, когда отписывается последний подписчик
        );

        foreach($orders as $order){
            $orderJson = json_encode($order);

            $msg = new AMQPMessage(
                $orderJson,
                array('delivery_mode' => 2) #создаёт сообщение постоянным, чтобы оно не потерялось при падении или закрытии сервера
            );

            $channel->basic_publish(
                $msg,           	#сообщение
                '',             	#обмен
                'order_queue' 	#ключ маршрутизации (очередь)
            );

        }

        $channel->close();
        $connection->close();

    }


} 