<?php
/**
 * Created by PhpStorm.
 * User: dx
 * Date: 01.06.15
 * Time: 21:32
 */

namespace Ristretto\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;


class OrderWorkerCommand extends Command
{
    protected $filename = NULL;

    protected function configure()
    {
        $this
            ->setName('order:worker')
            ->setDescription('Recive order from queue')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $connection = new AMQPConnection('localhost', 5672, 'guest', 'guest');
        $channel = $connection->channel();

        $channel->queue_declare(
            'order_queue',	#queue name - Имя очереди может содержать до 255 байт UTF-8 символов
            false,      	#passive - может использоваться для проверки того, инициирован ли обмен, без того, чтобы изменять состояние сервера
            true,      	#durable - убедимся, что RabbitMQ никогда не потеряет очередь при падении - очередь переживёт перезагрузку брокера
            false,      	#exclusive - используется только одним соединением, и очередь будет удалена при закрытии соединения
            false       	#autodelete - очередь удаляется, когда отписывается последний подписчик

        );

        /**
         * не отправляем новое сообщение на обработчик, пока он
         * не обработал и не подтвердил предыдущее. Вместо этого
         * направляем сообщение на любой свободный обработчик
         */
        $channel->basic_qos(
            null,   #размер предварительной выборки - размер окна предварительнйо выборки в октетах, null означает “без определённого ограничения”
            1,  	#количество предварительных выборок - окна предварительных выборок в рамках целого сообщения
            null	#глобальный - global=null означает, что настройки QoS должны применяться для получателей, global=true означает, что настройки QoS должны применяться к каналу
        );

        /**
         * оповещает о своей заинтересованности в получении
         * сообщений из определённой очереди. В таком случае мы
         * говорим, что они регистрируют получателя, или устанавливают
         * подписку на очередь. Каждый получатель (подписка) имеет
         * идентификатор, называемый “тег получателя”.
         */
        $channel->basic_consume(
            'order_queue',    	#очередь
            '',                  #тег получателя - Идентификатор получателя, валидный в пределах текущего канала. Просто строка
            false,               #не локальный - TRUE: сервер не будет отправлять сообщения соединениям, которые сам опубликовал
            false,               #без подтверждения - false: подтверждения включены, true - подтверждения отключены. отправлять соответствующее подтверждение обработчику, как только задача будет выполнена
            false,                 #эксклюзивная - к очереди можно получить доступ только в рамках текущего соединения
            false,                 #не ждать - TRUE: сервер не будет отвечать методу. Клиент не должен ждать ответа
            array($this, 'process')	#функция обратного вызова - метод, который будет принимать сообщение
        );

        while(count($channel->callbacks)) {
            $output->writeln('Order receive '.getmypid());
            $channel->wait();
        }

        $channel->close();
        $connection->close();

    }

    public function process(AMQPMessage $msg)
    {
        $this->save($msg->body);

        sleep(mt_rand(2, 5));

        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);

    }

    protected function save($order)
    {
        file_put_contents($this->getFileName(), $order."\n", FILE_APPEND);
    }

    protected function getFileName()
    {
        if(is_null($this->filename)){
            $this->filename = APP_PATH."/data/prepare_order_".getmypid().".log";
        }
        return $this->filename;
    }
} 